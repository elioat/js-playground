const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

let width;
let height;

const resize = function () {
    width = window.innerWidth * 2;
    height = window.innerHeight * 2;
    canvas.width = width;
    canvas.height = height;
};

window.onresize = resize;

resize();

const coin = {
    state: {
        x: width / 4,
        y: height / 5,
        isCollected: false,
    },
    draw: function() {
        if (!this.state.isCollected) {
            ctx.fillRect(this.state.x - 10, this.state.y - 10, 100, 100);
        }
    },
    update: function() {
        // console.log(this.state.isCollected);
    }
};

const player = {
    state: {
        x: width / 2,
        y: height / 2,
        pressedKeys: {
            left: false,
            right: false,
            up: false,
            down: false
        }
    },
    name: 'Bill the square.',
    color: 'teal',
    isVerticallyAligned: false,
    isHorizontallyAligned: false,
    draw: function() {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.state.x - 10, this.state.y - 10, 30, 30);
        ctx.font = '30px Helvetica';
        // ctx.fillText(this.name, this.state.x - 5, this.state.y - 20);
        ctx.fillText("v: " + this.isVerticallyAligned, this.state.x + 30, this.state.y - 15);
        ctx.fillText("h: " + this.isHorizontallyAligned, this.state.x + 30, this.state.y + 15);
    },
    update: function(progress) {
        if (this.state.pressedKeys.left) {
            this.state.x -= progress;
        }
        if (this.state.pressedKeys.right) {
            this.state.x += progress;
        }
        if (this.state.pressedKeys.up) {
            this.state.y -= progress;
        }
        if (this.state.pressedKeys.down) {
            this.state.y += progress;
        }
        if (this.state.x > width) {
            this.state.x -= width;
        } else if (this.state.x < 0) {
            this.state.x += width;
        }
        if (this.state.y > height) {
            this.state.y -= height;
        } else if (this.state.y < 0) {
            this.state.y += height;
        }
        let left = this.state.x;
        let right = this.state.x + width;
        let top = this.state.y;
        let bottom = this.state.y + height;
        if (left < coin.state.x && coin.state.x < right) {
            this.isVerticallyAligned = true;
        } else {
            this.isVerticallyAligned = false;
        }
        if (top < coin.state.y && coin.state.y < bottom) {
            this.isHorizontallyAligned = true;
        } else {
            this.isHorizontallyAligned = false;
        }
        if (this.isHorizontallyAligned === true && this.isVerticallyAligned === true) {
            coin.state.isCollected = true;
        }
    }
};

function update(progress) {
    player.update(progress);
    coin.update();
}

function draw() {
    ctx.clearRect(0, 0, width, height);
    player.draw();
    coin.draw();
}

function loop(timestamp) {
    let progress = timestamp - lastRender;
    update(progress);
    draw();
    lastRender = timestamp;
    window.requestAnimationFrame(loop);
}

let lastRender = 0;

window.requestAnimationFrame(loop);

const keyMap = {
    68: "right",
    65: "left",
    87: "up",
    83: "down"
};

function keydown(event) {
    let key = keyMap[event.keyCode];
    player.state.pressedKeys[key] = true;
}

function keyup(event) {
    let key = keyMap[event.keyCode];
    player.state.pressedKeys[key] = false;
}

window.addEventListener("keydown", keydown, false);
window.addEventListener("keyup", keyup, false);
